package com.sample.app.weatherapp.ui.splash

import com.sample.app.weatherapp.R
import com.sample.app.weatherapp.core.BaseFragment
import com.sample.app.weatherapp.core.Constants
import com.sample.app.weatherapp.databinding.FragmentSplashBinding
import com.sample.app.weatherapp.di.Injectable
import io.reactivex.disposables.CompositeDisposable
import androidx.navigation.fragment.findNavController


class SplashFragment : BaseFragment<SplashFragmentViewModel, FragmentSplashBinding>(SplashFragmentViewModel::class.java), Injectable {

    var disposable = CompositeDisposable()

    override fun getLayoutRes() = R.layout.fragment_splash

    override fun initViewModel() {
        mBinding.viewModel = viewModel
    }

    override fun init() {
        super.init()


        if (viewModel.sharedPreferences.getString(Constants.Coords.LON, "").isNullOrEmpty()) {
            findNavController().graph.startDestination = R.id.dashboardFragment
            navigate(R.id.action_splashFragment_to_dashboardFragment)
        } else {
            navigate(R.id.action_splashFragment_to_searchFragment)
        }

        mBinding.buttonExplore.setOnClickListener {
            navigate(R.id.action_splashFragment_to_searchFragment)
        }

    }


    override fun onDestroy() {
        super.onDestroy()
        disposable.clear()
    }
}
