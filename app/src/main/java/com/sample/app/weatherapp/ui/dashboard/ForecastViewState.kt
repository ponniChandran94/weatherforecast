package com.sample.app.weatherapp.ui.dashboard

import com.sample.app.weatherapp.db.entity.ForecastEntity
import com.sample.app.weatherapp.utils.domain.Status


class ForecastViewState(
    val status: Status,
    val error: String? = null,
    val data: ForecastEntity? = null
) {
    fun getForecast() = data

    fun isLoading() = status == Status.LOADING

    fun getErrorMessage() = error

    fun shouldShowErrorMessage() = error != null
}
