package com.sample.app.weatherapp.core

import androidx.lifecycle.ViewModel


open class BaseViewModel : ViewModel()
