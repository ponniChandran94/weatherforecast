package com.sample.app.weatherapp.ui.search

import com.sample.app.weatherapp.db.entity.CitiesForSearchEntity
import com.sample.app.weatherapp.utils.domain.Status

class SearchViewState(
    val status: Status,
    val error: String? = null,
    val data: List<CitiesForSearchEntity>? = null
) {
    fun isLoading() = status == Status.LOADING

    fun getErrorMessage() = error

    fun shouldShowErrorMessage() = error != null
}
